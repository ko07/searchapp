/*
 * requirements
 *
**/

var http = require('http');
var search = require('./search');
var express = require('express');
var bodyParser = require('body-parser');

/*
 * initiate
 *
**/

var app = express();

app.use(bodyParser.urlencoded ({ extended: true }));
app.use(bodyParser.json());
app.set('view engine', 'ejs');


/*
 * basic routes
 * 
**/

app.get("/", search.onRequest);
app.post("/", search.onFormSubmit);



/*
 * creating server
 *
**/

http.createServer(app).listen(1337, 'localhost'); console.log('Server is on');
