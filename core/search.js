/*
 * requirements
 *
**/

var select = require('./select');
var express = require('express');
var router = express.Router();

/*
 * module functions
 *
**/

module.exports = {

    //GET
    onRequest: function (request, response) {

        response.render("index.ejs", {
            title: 'Search',
            result: false,
            seller: '',
            type: '',
            weight: '',
            price: ''
        });
    },

    //POST
    onFormSubmit: function (request, response) {

        postData = {

            seller: request.body.seller,
            type: request.body.type,
            weight: request.body.weight,
            price: request.body.price
        }

        console.log(postData.seller + ' ' + postData.type + ' ' + postData.weight + ' ' + postData.price);
        
        select.sendQuery(postData.seller, postData.type, postData.weight, postData.price, function(result){
            
            console.log("from search module: " + result);

            //TODO: Parse array of result object and use it to render ejs

            response.render("index.ejs", {

                title: 'Search',
                result: true,
                seller: JSON.stringify(postData.seller),
                type: JSON.stringify(postData.type),
                weight: JSON.stringify(postData.weight),
                price: JSON.stringify(postData.price)
            });
        });
    }
}
