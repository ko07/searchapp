/*
 * requirements
 *
**/

var db = require('./dbconnect');
var mysql = require('mysql');

module.exports = {

	sendQuery: function(seller, type, weight, price, callback) {

		var connection = mysql.createConnection({
		    host: db.host,
		    user: db.user,
		    password: db.password,
		    database: db.database,
		    multipleStatements: db.multipleStatements,
		    socketPath: db.socketPath
		});

	    connection.connect(function(error){

		    if(!error) {    
		        console.log('connected!');
		    } else {
		        console.log(error);
		    }
		});

	    //TODO: Create module to build queries

	    var query = connection.query('select * from products where type = ?', type, function(error, result){
	    	
	    	if (error)
	    		console.error(error);

	    	else {

		    	result = JSON.stringify(result);

		    	callback(result); 	
	    	}
	    });


	}
}